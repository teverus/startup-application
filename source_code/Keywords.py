import os
from functools import wraps
from pathlib import Path
from time import sleep

import psutil
import pyautogui
from pygetwindow import Win32Window


def show_actions(argument):
    def decorator(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            app_name = os.getenv("APP_NAME")
            os.system(f"echo {app_name} :: {argument}")
            function(*args, **kwargs)
            os.system(f"echo {app_name} :: Done")

        return wrapper

    return decorator


class Keywords:
    def __init__(self):
        self.app_name = None

    def __get_app_window(
        self,
        title,
        ignore=None,
        wait_for=60,
        optional=False,
    ):
        ignored_titles = ignore if isinstance(ignore, list) else [ignore]
        ignored_titles = ignored_titles if ignore else None

        for _ in range(wait_for):
            windows = pyautogui.getWindowsWithTitle(title)
            for window in windows:
                window_title = window.title
                if ignored_titles and window_title in ignored_titles:
                    sleep(1)
                elif title in window_title:
                    return windows[0]
            sleep(1)
        else:
            if not optional:
                err = f"Couldn't find a window with title {self.app_name}"
                raise Exception(err)

    @show_actions("Starting")
    def start(self, path_to_executable):
        args = path_to_executable.split()
        path = Path(args[0])

        if path.exists():
            psutil.Popen(args)
        else:
            raise Exception(f"\n'{path_to_executable}' is not a valid path!")

    @show_actions("Minimizing")
    def minimize(self, title, ignore_titles=None):
        app_window: Win32Window = self.__get_app_window(title, ignore_titles)

        app_window.minimize()

        err = f"{self.app_name} window was NOT minimized"
        assert app_window.isMinimized, err

    @show_actions("Closing")
    def close(
        self,
        title,
        wait_for=30,
        stop_if_window_exists=None,
        optional=False,
        ignore=None,
    ):
        if stop_if_window_exists:
            stop_win_title = stop_if_window_exists
            get_win = self.__get_app_window
            win = get_win(stop_win_title, wait_for=wait_for, optional=True)
            if win:
                return

        window = self.__get_app_window(title, ignore, wait_for, optional)

        if window:
            window.close()

    @show_actions("Waiting for")
    def wait_for(self, timeout):
        sleep(timeout)

    @show_actions("Killing")
    def kill(self, target_ps):
        target_ps = target_ps if isinstance(target_ps, list) else [target_ps]
        for process in psutil.process_iter():
            p_name = process.name()
            match = [p_name == target_p for target_p in target_ps]
            if any(match):
                try:
                    process.terminate()
                except psutil.NoSuchProcess:
                    pass

    @show_actions("Executing")
    def execute(self, command):
        os.system(command)
