from pathlib import Path

import yaml

SETTINGS = "Settings"


class YamlTool:
    @classmethod
    def create_a_config_file(cls, file_name: str = "config", path: str = Path.cwd()):
        file_name_full = f"{file_name}.yaml"
        file_path = Path(path) / file_name_full
        file_path.touch(exist_ok=True)
        data = {SETTINGS: {"hello": "world"}}
        with open(file_path, "w") as file:
            yaml.dump(data, file, sort_keys=False)

    def __init__(self, path_to_config_file: str = "config.yaml"):
        self.path_to_config_file = Path(path_to_config_file)

    def get_config(self):
        with open(self.path_to_config_file, "r", encoding="utf-8") as file:
            data = yaml.safe_load(file)

        return data

    def get_settings(self):
        config = self.get_config()
        settings = config[SETTINGS]

        return settings

    def save_settings(self, data: dict):
        with open(self.path_to_config_file, "w", encoding="utf-8") as file:
            yaml.dump(data, file, sort_keys=False, allow_unicode=True)

    def update_a_setting(self, setting_name, new_value, config_section=SETTINGS):
        config = self.get_config()
        settings = config[config_section]
        settings[setting_name] = new_value
        self.save_settings(config)
