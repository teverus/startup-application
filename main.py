import os
import sys
from time import sleep

from source_code.constants import APPLICATIONS, DOUBLE_CHECK, TIMEOUT
from source_code.Keywords import Keywords
from source_code.teverus_sdk.YamlTool import YamlTool


class Application(Keywords):
    def __init__(self):
        super().__init__()

        # region --- Variables -------------------------------------------------

        self.file_to_read = sys.argv[1]
        self.config = YamlTool(self.file_to_read).get_config()

        # endregion ------------------------------------------------------------

        # region === MAIN LOOP =================================================

        args = "The following applications will be started:", self.config
        self.show_welcome_message(*args)

        for app_name, app_info in self.config[APPLICATIONS].items():
            os.environ["APP_NAME"] = app_name
            self.app_name = app_name
            self.app_info = app_info

            for keyword_name, parameters in self.app_info.items():
                if keyword_name == DOUBLE_CHECK:
                    continue

                self.execute_keyword(keyword_name, parameters)

        # region --- Double check loop -----------------------------------------

        for app_name, app_info in self.config[APPLICATIONS].items():
            os.environ["APP_NAME"] = app_name
            self.app_name = app_name
            self.app_info = app_info

            if DOUBLE_CHECK not in self.app_info:
                continue

            actions = self.app_info[DOUBLE_CHECK]
            actions = actions if isinstance(actions, list) else [actions]

            for action in actions:
                keyword_name = action
                parameters = self.app_info[action]
                self.execute_keyword(keyword_name, parameters)

        # endregion ------------------------------------------------------------

        # endregion ============================================================

    # region --- Methods -------------------------------------------------------

    @staticmethod
    def show_welcome_message(title_message, config):
        timeout = config[TIMEOUT]
        app_names = config[APPLICATIONS].keys()

        title_message = f"{title_message}\n"
        for app_name in app_names:
            title_message = f"{title_message}  - {app_name}\n"

        for index in range(timeout):
            os.system("cls")
            print(title_message)
            print(f"This will happen in {timeout - index}")
            sleep(1)

        os.system("cls")

    def get_keyword(self, keyword_name):
        try:
            keyword = self.__getattribute__(keyword_name)

            return keyword
        except AttributeError:
            err = f"'{keyword_name}' keyword is not implemented :("
            raise Exception(err)

    @staticmethod
    def get_arguments(parameters):
        if isinstance(parameters, list) and isinstance(parameters[0], dict):
            kwargs = {}
            for element in parameters:
                kwargs = {**kwargs, **element}
            keyword_arguments = kwargs
        else:
            keyword_arguments = parameters

        return keyword_arguments

    def execute_keyword(self, keyword_name, parameters):
        keyword = self.get_keyword(keyword_name)
        arguments = self.get_arguments(parameters)
        if isinstance(arguments, dict):
            keyword(**arguments)
        else:
            keyword(arguments)

    # endregion ----------------------------------------------------------------


if __name__ == "__main__":
    Application()
